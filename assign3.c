#include <stdio.h>

int square(int a) {
	return a * a;
}

int factorial(int n) {
	int i, r = 1;
	for(i=1; i<=n; i++)
		r = r * i;
	return r;
}

int divide(int a, int b) {
	return a / b;
}

int main() {
	int num, res;
	printf("enter a number: ");
	scanf("%d", &num);
	//res = square(num);
	//res = factorial(num);
	res = divide(num, num);
	printf("result: %d\n", res);
	return 0;
}

